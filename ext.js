function enableInputs(b){
	b = b || false
	$('#filename').prop("disabled", !b );
	$('#btn_export').prop("disabled", !b);
	$('#select_preset').prop("disabled", !b);
}

function resetState(){
	currentRenderingFilePath = undefined
	currentEncoderJobID = undefined
	currentItemID = undefined
	currentProgress = undefined
	currentFileID = undefined
	currentImportJobID = undefined
	// $('#filename').val('')
	enableInputs(true)
}

function onLoaded() {
	logger.info('begin','onLoaded')
	os = require('os')
	if(Raven){
		Raven.config('https://025779c341b44568a15d7b42b353ef6b@sentry.io/1283760',{
			environment: environment
		}).install()
	}
	var userCtx = {
		environment: environment,
		hostname: os.hostname(),
		networkInterfaces: os.networkInterfaces(),
		platform: os.platform(),
		release: os.release(),
		uptime: os.uptime()
	}
	Raven.setUserContext(userCtx)
	logger.info('user context','onLoaded',userCtx)
	axiosRetry(axios, { retries: 3, shouldResetTimeout: true })
	csInterface = new CSInterface();
	fs = require('fs')
	path = require('path')
	requestor = axios.create({
		baseURL: 'http://mam.comm.local:8080/API/',
		timeout: 10000,
		auth: {
			username: 'editor',
			password: 'editor'
		},
	});
	storageID = 'COW-16';
	storagePath = '/Volumes/smalltree/';
	portalUrl = 'http://mam.comm.local/'
	metadataGroup = "Media"
	resetState()
	$('#btn_export').prop("disabled", true);
	
	
	var appName = csInterface.hostEnvironment.appName;
	var appVersion = csInterface.hostEnvironment.appVersion;
	
	// document.getElementById("dragthing").style.backgroundColor = "lightblue";
	var caps = csInterface.getHostCapabilities();
	
	loadJSX();
	
	setPresetsList()

	updateThemeWithAppSkinInfo(csInterface.hostEnvironment.appSkinInfo);

	// Update the color of the panel when the theme color of the product changed.
	csInterface.addEventListener(CSInterface.THEME_COLOR_CHANGED_EVENT, onAppThemeColorChanged);
	// Listen for event sent in response to rendering a sequence.
	csInterface.addEventListener("com.adobe.csxs.events.PProPanelRenderEvent", handleRenderEvent);

	// register for messages
	// VulcanInterface.addMessageListener(
	//     VulcanMessage.TYPE_PREFIX + "com.DVA.message.sendtext",
	//     function(message) {
	//         var str = VulcanInterface.getPayload(message);
	//         // You just received the text of every Text layer in the current AE comp.
	//     }
	// );
	// csInterface.evalScript('$._PPP_.getVersionInfo()', myVersionInfoFunction);	
	// csInterface.evalScript('$._PPP_.getActiveSequenceName()', myCallBackFunction);		
	// csInterface.evalScript('$._PPP_.getUserName()', myUserNameFunction);  
	// csInterface.evalScript('$._PPP_.getSequenceProxySetting()', myGetProxyFunction);
	csInterface.evalScript('$._PPP_.keepPanelLoaded()');
	// csInterface.evalScript('$._PPP_.disableImportWorkspaceWithProjects()');
	// register project item selected callback
	// csInterface.evalScript('$._PPP_.registerProjectPanelChangedFxn()');
}

function setPresetsList(){
	try {
		var presetPath = path.join(extensionPath,'payloads')
		var allFiles = fs.readdirSync(presetPath)
		logger.info('allFiles','setPresetsList',allFiles)
		var presetFiles = allFiles.filter(function(item){
			return item.endsWith('.epr')
		}).map(function(val){
			return val.substring(0,val.length-4)
		}).forEach(function(item){
			$('#select_preset').append($("<option></option>")
											.attr("value",item)
											.text(item));
		})
		$('#select_preset').val('XDCAM EX420 35Mbps')
	} catch (e) {
		logger.error(e,'setPresetsList')
		alert(e)
	}
	
}

function oninputFilename() {
	var val = $('#filename').val();
	// alert(val)
	if (val.length > 0) {
		$('#btn_export').prop("disabled", false);
	} else {
		$('#btn_export').prop("disabled", true);
	}
}

function createPlaceholder(title) {
	addLog('Attempting to create placeholder in Portal')
	var req = requestor.post('import/placeholder?container=1',{
    "timespan": [{
            "field": [{
                    "name": "title",
                    "value": [{
                            "value": title
                        }]
                }],
            "start": "-INF",
            "end": "+INF"
        }]
})
	return req.then(function(res){
		return Promise.resolve(res.data.id)
	})
}

function notifyFile(path) {
	var req = requestor.post('storage/'+storageID+'/file?createOnly=false&path='+decodeURI($('#filename').val()+'.mxf'))
	return req.then(function(res){
		return Promise.resolve(res.data.id)
	})
}

function notifyFileClosed(fileId){
	var req = requestor.put('storage/file/'+fileId+'/state/CLOSED')
	return req.then(function(res){
		logger.info('result','notifyFileClosed',res)
		return res
	})
}

function checkMetadataCompleted(itemID){
	var req = requestor.get('item/'+itemID+'/metadata;group='+metadataGroup)
		return req.then(function(res){
			if(!res.data.item[0].metadata.group){
				addLog('Metadata for '+makePortalLink(itemID)+' HAS NOT been entered into portal.<br />Click link above to create and save metadata.')
				alert('Render has completed but metadata has not been entered for Item '+itemID)
			}else{
				addLog('Metadata for '+makePortalLink(itemID)+' has been succesfully entered into Portal.')
			}
			logger.info('result','checkMetadataCommpleted',res)
			return res
		})
}

function importFile(fileID, itemID) {
	var req = requestor.post('import/placeholder/'+itemID+'/container?fileId='+fileID+'&tag=lowres&growing=true')
	return req.then(function(res){
		logger.info('result','importFile',res)
		return res
	})
}

function deleteFiles() {
	if(currentRenderingFilePath){
		try {
			if(fileExists(currentRenderingFilePath)){
				fs.unlink(currentRenderingFilePath)
			}else{
				logger.warn('does not exists, not deleting','deleteFiles',currentRenderingFilePath)
			}
			if(fileExists(currentRenderingFilePath+'.xmp')){
				fs.unlink(currentRenderingFilePath+'.xmp')
			}else{
				logger.warn('does not exists, not deleting','deleteFiles',currentRenderingFilePath+'.xmp')
			}
			addLog('Files deleted.')
		} catch (e) {
			logger.error(e,'deleteFiles')
		}
	}else{
		logger.warn('currentRenderingFilePath undefined...will not try to delete', 'deleteFiles')
	}
	if(currentFileID){
		return requestor.delete('storage/file/'+currentFileID)
		.catch(function(e){
			logger.error(e,'deleteFiles')
		})
		.then(function(r){
			logger.info('success delete storage/file/'+currentFileID,'deleteFiles',r)
			return r
		})
	}else{
		logger.warn('no currentFileID skipping vidispine delete','deleteFiles')
		return Promise.resolve(true)
	}
	
}

function btn_createSeqClicked() {
	var presetFile = path.join(extensionPath,'payloads/',$('#select_preset').val()+'.sqpreset')
	logger.info('user button click','btn_createSeqClicked',presetFile)
	evalScript("$._PPP_.createSequenceFromPreset('"+ presetFile +"','"+$('#select_preset').val()+"')")
}

function btn_exportClicked() {
	enableInputs(false)
	$('#filename').val($('#filename').val().replace(/ /g,'_'))
	var filename = $('#filename').val()+'.mxf'
	logger.info('user button click','btn_exportClicked',filename)
	var joined = path.join(storagePath, filename)
	if(/[^a-zA-Z0-9_@-]/i.test($('#filename').val())){
		resetState()
		logger.warn('bad user input','btn_exportClicked',filename)
		alert('No periods or special characters except @ _ - are allowed.  Please revise your file name and submit again.')
		return false
	}

	if(fileExists(joined)){
		resetState()
		logger.warn('file already exists','btn_exportClicked',filename)
		alert('The file ' + filename + ' already exists on the network storage.  Choose another file name.','warning')
		return false
	}
	presetFile = path.join(extensionPath,'/payloads/',$('#select_preset').val()+'.epr')
	currentRenderingFilePath = joined
	$('#logList').empty()
	addLog('Attempting to queue the job.')
	evalScript("$._PPP_.render('" + joined + "','"+ presetFile +"')",function(ret){
		currentEncoderJobID = ret
	})
};

function handleRenderEvent(event){
	if(event.data.jobId === currentEncoderJobID){
		logger.info('event received',handleRenderEvent,event)
		switch (event.data.type) {
			case 'onEncoderJobQueued':
				addLog('Render job successfully queued in Media Encoder.')
				break;
			case 'onEncoderJobProgress':
				if(event.data.progerss != 0.0){
					if(currentProgress === undefined){
						logger.info('render started','handleRenderEvent')
						addLog('Rendering has started.')
						lookForGrowingFile(currentRenderingFilePath)
							.then(function(){
								addLog('File detected on network storage.')
								return createPlaceholder($('#filename').val())
							})
							.then(function(id){
								currentItemID = id
								addLog('Portal Media Item: '+makePortalLink(id)+' has been created.')
								return notifyFile(currentRenderingFilePath)
							})
							.then(function(fileID){
								currentFileID = fileID
								addLog('File entity created.')
								return importFile(fileID, currentItemID)
							})
							.then(function(){
								addLog('Portal import job started.')
								confirm('Click Yes to open a browser window with this new Portal Media Item then click "Create Metadata" on the right side of the screen to enter your metadata',function(result){
									logger.info('enter metadata confirm result','handleRenderEvent',result)
									openItemInBrowser(currentItemID) 	
								})
							})
							.catch(function(e){
								addLog('Error: '+e)
								logger.error(e,'handleRenderEvent')
								alert(e)
							})
					}
					currentProgress = event.data.progress
				}
				break;
			case 'onEncoderJobCanceled':
				deleteFiles().then(function(){
						resetState()
					})
				addLog('Rendering job was cancelled by user.')
				// alert('Render job was cancelled!')
				break;
			case 'onEncoderJobError':
				deleteFiles().then(function(){
					resetState()
				})
				addLog('An error occured while rendering: '+event.data.error)
				logger.error(event,'handleRenderEvent')
				alert('An error occured while rendering: '+event.data.error+'.  The render has most likely stopped.')
				break;
			case 'onEncoderJobComplete':
				addLog('Rendering complete: '+$('#filename').val()+'.mxf')
				notifyFileClosed(currentFileID)
				checkMetadataCompleted(currentItemID)
				resetState()
				break;
			default:
				break;
		}
	}else{
		logger.warn('received an event from an unkown job ID','handleRenderEvent',event)
	}
}

function fileExists(path){
	try {
		fs.accessSync(path)
		return true
	} catch (error) {
		return false
	}
}

function lookForGrowingFile(filepath) {
	try {
		var firstStats = fs.statSync(filepath)
	} catch (error) {
		logger.error(error,'lookForGrowingFile')
		return Promise.reject(new Error('The file cannot be found on the network storage!'))
	}
	return new Promise(function (resolve, reject) {
		var count = 0
		var countLimit = 8
		var doResolve = function(){
			if (interval){
				clearInterval(interval)
			}
			resolve()
		}

		doReject = function(reason){
			if (interval){
				clearInterval(interval)
			}
			reject(reason)
		}

		var interval = setInterval(function () {
			try {
				var secondStats = fs.statSync(filepath)
			} catch (error) {
				reject(error)
			}
			if (firstStats.size === secondStats.size) {
				if(count > countLimit){
					doReject('File is not growing')
					return
				}else{
					count++
				}
			} else {
				doResolve()
			}
		}, 500)
	})
}

function openItemInBrowser(itemID){
	csInterface.openURLInDefaultBrowser(portalUrl+'vs/item/'+itemID+'/')
}

function makePortalLink(itemID){
	var url = portalUrl + 'vs/item/' + itemID + '/'
	return '<a href="#" onclick="openItemInBrowser(\''
		+itemID
		+'\')">'
		+itemID
		+'</a>'
}

function addLog(message) {
	$('#logList').append('<li>' + message + '</li>');
};

function alert(message) {
	evalScript("alert('" + message +"')")
}

function toast(message, level){
	level = level || 'info'
	evalScript("app.setSDKEventMessage('" + message +"','"+level+"')")
}

function confirm(message,callback){
	evalScript("confirm('" + message +"')",callback)
}



/**
 * Update the theme with the AppSkinInfo retrieved from the host product.
 */

function updateThemeWithAppSkinInfo(appSkinInfo) {
	//Update the background color of the panel

	var panelBackgroundColor = appSkinInfo.panelBackgroundColor.color;
	document.body.bgColor = toHex(panelBackgroundColor);

	var styleId 			= "ppstyle";
	var gradientBg			= "background-image: -webkit-linear-gradient(top, " + toHex(panelBackgroundColor, 40) + " , " + toHex(panelBackgroundColor, 10) + ");";
	var gradientDisabledBg	= "background-image: -webkit-linear-gradient(top, " + toHex(panelBackgroundColor, 15) + " , " + toHex(panelBackgroundColor, 5) + ");";
	var boxShadow			= "-webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.4), 0 1px 1px rgba(0, 0, 0, 0.2);";
	var boxActiveShadow		= "-webkit-box-shadow: inset 0 1px 4px rgba(0, 0, 0, 0.6);";
		 
	var isPanelThemeLight	= panelBackgroundColor.red > 50; // choose your own sweet spot
		 
		var fontColor, disabledFontColor;
		var borderColor;
		var inputBackgroundColor;
		var gradientHighlightBg;

		if(isPanelThemeLight) {
			fontColor				= "#000000;";
			disabledFontColor		= "color:" + toHex(panelBackgroundColor, -70) + ";";
			borderColor				= "border-color: " + toHex(panelBackgroundColor, -90) + ";";
			inputBackgroundColor	= toHex(panelBackgroundColor, 54) + ";";
			gradientHighlightBg		= "background-image: -webkit-linear-gradient(top, " + toHex(panelBackgroundColor, -40) + " , " + toHex(panelBackgroundColor,-50) + ");";
		} else {
			fontColor				= "#ffffff;";
			disabledFontColor		= "color:" + toHex(panelBackgroundColor, 100) + ";";
			borderColor				= "border-color: " + toHex(panelBackgroundColor, -45) + ";";
			inputBackgroundColor	= toHex(panelBackgroundColor, -20) + ";";
			gradientHighlightBg		= "background-image: -webkit-linear-gradient(top, " + toHex(panelBackgroundColor, -20) + " , " + toHex(panelBackgroundColor, -30) + ");";
		}
	
		//Update the default text style with pp values

		addRule(styleId, ".default", "font-size:" + appSkinInfo.baseFontSize + "px" + "; color:" + fontColor + "; background-color:" + toHex(panelBackgroundColor) + ";");
		addRule(styleId, "button, select, input[type=text], input[type=button], input[type=submit]", borderColor);	   
		addRule(styleId, "p", "color:" + fontColor + ";");	  
		addRule(styleId, "button", "font-family: " + appSkinInfo.baseFontFamily + ", Arial, sans-serif;");	  
		addRule(styleId, "button", "color:" + fontColor + ";");	   
		addRule(styleId, "button", "font-size:" + (1.2 * appSkinInfo.baseFontSize) + "px;");	
		addRule(styleId, "button, select, input[type=button], input[type=submit]", gradientBg);	
		addRule(styleId, "button, select, input[type=button], input[type=submit]", boxShadow);
		addRule(styleId, "button:enabled:active, input[type=button]:enabled:active, input[type=submit]:enabled:active", gradientHighlightBg);
		addRule(styleId, "button:enabled:active, input[type=button]:enabled:active, input[type=submit]:enabled:active", boxActiveShadow);
		addRule(styleId, "[disabled]", gradientDisabledBg);
		addRule(styleId, "[disabled]", disabledFontColor);
		addRule(styleId, "input[type=text]", "padding:1px 3px;");
		addRule(styleId, "input[type=text]", "background-color: " + inputBackgroundColor + ";");
		addRule(styleId, "input[type=text]:focus", "background-color: #ffffff;");
		addRule(styleId, "input[type=text]:focus", "color: #000000;");
}

function addRule(stylesheetId, selector, rule) {
	var stylesheet = document.getElementById(stylesheetId);
	
	if (stylesheet) {
		stylesheet = stylesheet.sheet;
		if( stylesheet.addRule ){
				stylesheet.addRule(selector, rule);
		} else if( stylesheet.insertRule ){
			stylesheet.insertRule(selector + ' { ' + rule + ' }', stylesheet.cssRules.length);
		}
	}
}

function reverseColor(color, delta) {
	return toHex({red:Math.abs(255-color.red), green:Math.abs(255-color.green), blue:Math.abs(255-color.blue)}, delta);
}

/**
 * Convert the Color object to string in hexadecimal format;
 */

function toHex(color, delta) {
	function computeValue(value, delta) {
		var computedValue = !isNaN(delta) ? value + delta : value;
		if (computedValue < 0) {
			computedValue = 0;
		} else if (computedValue > 255) {
			computedValue = 255;
		}

		computedValue = Math.round(computedValue).toString(16);
		return computedValue.length == 1 ? "0" + computedValue : computedValue;
	}

	var hex = "";
	if (color) {
		hex = computeValue(color.red, delta) + computeValue(color.green, delta) + computeValue(color.blue, delta);
	}
	return "#" + hex;
}

function onAppThemeColorChanged(event) {
	// Should get a latest HostEnvironment object from application.
	var skinInfo = JSON.parse(window.__adobe_cep__.getHostEnvironment()).appSkinInfo;
	// Gets the style information such as color info from the skinInfo, 
	// and redraw all UI controls of your extension according to the style info.
	updateThemeWithAppSkinInfo(skinInfo);
} 

/**
* Load JSX file into the scripting context of the product. All the jsx files in 
* folder [ExtensionRoot]/jsx & [ExtensionRoot]/jsx/[AppName] will be loaded.
*/
function loadJSX() {

	// get the appName of the currently used app. For Premiere Pro it's "PPRO"
	var appName = csInterface.hostEnvironment.appName;
	extensionPath = csInterface.getSystemPath(SystemPath.EXTENSION);

	// load general JSX script independent of appName
	var extensionRootGeneral = extensionPath + '/jsx/';
	csInterface.evalScript('$._ext.evalFiles("' + extensionRootGeneral + '")');

	// load JSX scripts based on appName
	var extensionRootApp = extensionPath + '/jsx/' + appName + '/';
	csInterface.evalScript('$._ext.evalFiles("' + extensionRootApp + '")');
}

function evalScript(script, callback) {
	csInterface.evalScript(script, callback);
}

// function onClickButton(ppid) {z
// 	var extScript = "$._ext_" + ppid + ".run()";
// 	evalScript(extScript);
// }
