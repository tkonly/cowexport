$._PPP_={
	getVersionInfo : function() {
		return 'PPro ' + app.version + 'x' + app.build;
	},

	getUserName : function() {
		var	homeDir		= new File('~/');
		var	userName	= homeDir.displayName;
		homeDir.close();
		return userName;
	},

	keepPanelLoaded : function() {
		app.setExtensionPersistent("com.adobe.PProPanel", 0); // 0, while testing (to enable rapid reload); 1 for "Never unload me, even when not visible."
	},

	updateGrowingFile : function() {
		var numItems	= app.project.rootItem.children.numItems;
		for (var i = 0; i < numItems; i++){
			var currentItem = app.project.rootItem.children[i];
			if (currentItem){
				currentItem.refreshMedia();
			}
		}
	},

	getSep : function() {
		if (Folder.fs == 'Macintosh') {
			return '/';
		} else {
			return '\\';
		}
	},

	saveProject : function() {
		app.project.save();
	},

	exportCurrentFrameAsPNG : function() {
		app.enableQE();
		var activeSequence	= qe.project.getActiveSequence(); 	// note: make sure a sequence is active in PPro UI
		if (activeSequence) {
			var time			= activeSequence.CTI.timecode; 	// CTI = Current Time Indicator.

			var removeThese = /:|;/ig;    // Why? Because Windows chokes on colons.
			time = time.replace(removeThese, '_');
			var outputPath		= new File("~/Desktop");
			var outputFileName	= outputPath.fsName + $._PPP_.getSep() + time + '___' + activeSequence.name;

			activeSequence.exportFramePNG(time, outputFileName);
		} else {
			$._PPP_.updateEventPanel("No active sequence.");
		}
	},

	getActiveSequenceName : function() {
		if (app.project.activeSequence) {
			return app.project.activeSequence.name;
		} else {
			return "No active sequence.";
		}
	},
	
	registerProjectPanelChangedFxn : function() {
		success = app.bind("onSourceClipSelectedInProjectPanel", $._PPP_.projectPanelSelectionChanged);
	},

	projectPanelSelectionChanged : function() {
		var remainingArgs 	= arguments.length;
        var message 		= arguments.length + " items selected: ";
        
		for (var i = 0; i < arguments.length; i++) {
            message += arguments[i].name;
            remainingArgs--;
            if (remainingArgs > 1) {
                message += ', ';
            }
            if (remainingArgs === 1){
                message += ", and ";
            } 
            if (remainingArgs === 0) {
                message += ".";
            }
        }
        app.setSDKEventMessage(message, 'info');
    },

	getProjectPanelMeta : function() {
		$._PPP_.updateEventPanel(app.project.getProjectPanelMetadata());
	},

	setProjectPanelMeta : function() {
		metadata = "<?xml version='1.0'?><md.paths version='1.0'><metadata_path><internal>true</internal><namespace>http://ns.adobe.com/exif/1.0/</namespace><description>ColorSpace</description><entry_name>ColorSpace</entry_name><parent_id>http://ns.adobe.com/exif/1.0/</parent_id></metadata_path><metadata_path><internal>false</internal><namespace>http://amwa.tv/mxf/as/11/core/</namespace><description>audioTrackLayout</description><entry_name>audioTrackLayout</entry_name><parent_id>http://amwa.tv/mxf/as/11/core/</parent_id></metadata_path><metadata_path><internal>false</internal><namespace>http://ns.useplus.org/ldf/xmp/1.0/</namespace><description>ImageCreator</description><entry_name>ImageCreator</entry_name><parent_id>http://ns.useplus.org/ldf/xmp/1.0/</parent_id></metadata_path></md.paths>";
		app.project.setProjectPanelMetadata(metadata);
	},

	createSequenceFromPreset : function(presetPath,presetName) {
		app.enableQE();
		var seqName = prompt('Name of new '+presetName+ ' sequence?','Sequence');
		var sameNameFound = false
		if (seqName) {
			var allSequences = app.project.sequences;  
			for (var index = 0; index < allSequences.numSequences; index++) {  
    		var currentSequence = allSequences[index];  
				if(seqName === currentSequence.name){
					sameNameFound = true
				}
    	}  
			if(sameNameFound){
				alert('There is already a sequence with the name '+seqName+' in the project.  Please choose another name.')
			}else{
				qe.project.newSequence(seqName, presetPath);
			}
		}
	},

	render : function(renderToPath, presetPath) {
		// $._PPP_.updateEventPanel("eh?");
		app.enableQE();
		var activeSequence = qe.project.getActiveSequence();	// we use a QE DOM function, to determine the output extension.
		if (activeSequence)	{
			app.encoder.launchEncoder();	// This can take a while; let's get the ball rolling.

			var presetPathFile = new File(presetPath)
			
			//var timeSecs	= activeSequence.CTI.secs;		// Just for reference, here's how to access the CTI 
			//var timeFrames	= activeSequence.CTI.frames;	// (Current Time Indicator), for the active sequence. 
			//var timeTicks	= activeSequence.CTI.ticks;
			//var timeString	= activeSequence.CTI.timecode;

			//var seqInPoint	= app.project.activeSequence.getInPoint();	// new in 9.0
			//var seqOutPoint	= app.project.activeSequence.getOutPoint();	// new in 9.0

			// var projPath	= new File(app.project.path);
			// $._PPP_.updateEventPanel(projPath);
			// var outputPath  = Folder.selectDialog("Choose the output directory");

			// if ((outputPath) && projPath.exists){
			// 	var outPreset		= new File(outputPresetPath);
			// 	if (outPreset.exists === true){
			// 		var outputFormatExtension		=	activeSequence.getExportFileExtension(outPreset.fsName);
			// 		if (outputFormatExtension){
			// 			var outputFilename	= 	activeSequence.name + '.' + outputFormatExtension;

			// 			var fullPathToFile	= 	outputPath.fsName 	+ 
			// 									$._PPP_.getSep() 	+ 
			// 									activeSequence.name + 
			// 									"." + 
			// 									outputFormatExtension;			

			// 			var outFileTest = new File(fullPathToFile);

			// 			if (outFileTest.exists){
			// 				var destroyExisting	= confirm("A file with that name already exists; overwrite?", false, "Are you sure...?");
			// 				if (destroyExisting){
			// 					outFileTest.remove();
			// 					outFileTest.close();
			// 				}
			// 			}

						app.encoder.bind('onEncoderJobComplete',	$._PPP_.onEncoderJobComplete);
						app.encoder.bind('onEncoderJobError', 		$._PPP_.onEncoderJobError);
						app.encoder.bind('onEncoderJobProgress', 	$._PPP_.onEncoderJobProgress);
						app.encoder.bind('onEncoderJobQueued', 		$._PPP_.onEncoderJobQueued);
						app.encoder.bind('onEncoderJobCanceled',	$._PPP_.onEncoderJobCanceled);


			// 			// use these 0 or 1 settings to disable some/all metadata creation.

			// 			app.encoder.setSidecarXMPEnabled(0);
			// 			app.encoder.setEmbeddedXMPEnabled(0);

			// 			/* 

			// 			For reference, here's how to export from within PPro (blocking further user interaction).
						
			// 			var seq = app.project.activeSequence; 
						
			// 			if (seq) {
			// 				seq.exportAsMediaDirect(fullPathToFile,  
			// 										outPreset.fsName, 
			// 										app.encoder.ENCODE_WORKAREA);

			// 				Bonus: Here's how to compute a sequence's duration, in ticks. 254016000000 ticks/second.
			// 				var sequenceDuration = app.project.activeSequence.end - app.project.activeSequence.zeroPoint;						
			// 			}
						
			// 			*/
						app.encoder.setSidecarXMPEnabled(0);

						app.encoder.setEmbeddedXMPEnabled(0);
					 
					 
						var jobID = app.encoder.encodeSequence(	app.project.activeSequence,
																renderToPath,
																presetPathFile.fsName,
																app.encoder.ENCODE_WORKAREA, 
																0);	   // Remove from queue upon successful completion?					
						app.encoder.startBatch();
						presetPathFile.close();
						return jobID
			// 		}
			// 	} else {
			// 		$._PPP_.updateEventPanel("Could not find output preset.");
			// 	}
			// } else {
			// 	$._PPP_.updateEventPanel("Could not find/create output path.");
			// }
			// projPath.close();
		} else {
			// $._PPP_.updateEventPanel("No active sequence to render.");
			return 'No Active Sequence'
		}
	},

	updateEventPanel : function(message, level) {
		level = level || 'info'
		app.setSDKEventMessage(message, level);
		//app.setSDKEventMessage('Here is some information.', 'info');
		//app.setSDKEventMessage('Here is a warning.', 'warning');
		//app.setSDKEventMessage('Here is an error.', 'error');  // Very annoying; use sparingly.
	},

	
	clearCache : function () {
		app.enableQE();

		MediaType 	= {};

		// Magical constants from Premiere Pro's internal automation..

		MediaType.VIDEO = "228CDA18-3625-4d2d-951E-348879E4ED93";
		MediaType.AUDIO = "80B8E3D5-6DCA-4195-AEFB-CB5F407AB009";
		MediaType.ANY	= "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF";
		qe.project.deletePreviewFiles(MediaType.ANY);
		$._PPP_.updateEventPanel("All video and audio preview files deleted.");
	},
	
	// Define a couple of callback functions, for AME to use during render.
	
	message : function (msg) {
		//$.writeln(msg);	 // Using '$' object will invoke ExtendScript Toolkit, if installed.
	},
	
	sendRenderEvent : function(data){
		if (Folder.fs == 'Macintosh') {
			var eoName = "PlugPlugExternalObject";							
		} else {
			var eoName = "PlugPlugExternalObject.dll";
		}
		mylib	= new ExternalObject('lib:' + eoName);
		var eventObj	= new CSXSEvent();
		eventObj.type	= "com.adobe.csxs.events.PProPanelRenderEvent";
		eventObj.data	= JSON.lave(data);
		eventObj.dispatch();
	},

	onEncoderJobComplete : function (jobID, outputFilePath) {	
		$._PPP_.sendRenderEvent({
			type: 'onEncoderJobComplete',
			jobId: jobID,
			outputFilePath: outputFilePath
		})
	},

	onEncoderJobError : function (jobID, errorMessage) {
		$._PPP_.sendRenderEvent({
			type: 'onEncoderJobError',
			jobId: jobID,
			error: errorMessage
		})
	},
	
	onEncoderJobProgress : function (jobID, progress) {
		if(progress != 0.0 && $.global.currentJobID !== jobID){
			$.global.currentJobID = jobID
			$._PPP_.sendRenderEvent({
				type: 'onEncoderJobProgress',
				jobId: jobID,
				progress: progress
			})
		}
	},

	onEncoderJobQueued : function (jobID) {
		app.encoder.startBatch();
		$._PPP_.sendRenderEvent({
			type: 'onEncoderJobQueued',
			jobId: jobID
		})
	},

	onEncoderJobCanceled : function (jobID) {
		$._PPP_.sendRenderEvent({
			type: 'onEncoderJobCanceled',
			jobId: jobID
		})
	},
};
