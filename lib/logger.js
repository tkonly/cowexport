environment = 'production'

var _LTracker = _LTracker || [];

_LTracker.push({
  logglyKey: "848485c3-9d97-424b-9038-5b9f38c93ee9",
  // sendConsoleErrors: true,
  tag: "COWExport,"+environment
});

logger = {};

var formatLogMessage = function(level, fnName, message) {
  var obj = {
    level: level,
    timestamp: new Date(),
    message: message,
    fnName: fnName
  };
  if (arguments[3]) {
    obj.meta = arguments[3];
  }
  return obj;
};

var writeLogObject = function(obj) {
	if(environment && environment === 'development'){
		console.log(obj);
	}
  _LTracker.push(obj);
};

logger.info = function(message, fnName, obj) {
  writeLogObject(formatLogMessage("info", fnName, message, obj));
};
logger.warn = function(message, fnName, obj) {
  writeLogObject(formatLogMessage("warn", fnName, message, obj));
};
logger.debug = function(message, fnName, obj) {
  writeLogObject(formatLogMessage("debug", fnName, message, obj));
};
logger.error = function(e, fnName) {
	writeLogObject(formatLogMessage("error", fnName, e.message, e));
	Raven.captureException(e)
};

var oldOnError = window.onerror

window.onerror = function (msg, url, lineNo, columnNo, error) {
  logger.error(error,'onerror')
  if(oldOnError){
    oldOnError.apply(window,arguments)
  }
}

process.on('unhandledRejection', function(reason, p) {
  logger.error(reason,'unhandledRejection')
})

logger.info('COWExport page load start', 'logger.js')